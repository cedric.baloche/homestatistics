#include <cstdlib>
#include <memory>
#include <restbed>

void get_method_handler(const std::shared_ptr<restbed::Session>& session)
{
    const std::string response = "Hello, World!";
    session->close(restbed::OK, response, { { "Content-Length", std::to_string(response.length()) },{ "Connection", "close" } });
}

int main(const int, const char**)
{
    restbed::Service service;

    auto resource = std::make_shared<restbed::Resource>();
    resource->set_path("/resource");
    resource->set_method_handler("GET", get_method_handler);

    service.publish(resource);

    std::shared_ptr<restbed::Settings> settings = std::make_shared<restbed::Settings>();
    settings->set_port(8080);

    service.start(settings);

    return EXIT_SUCCESS;
}